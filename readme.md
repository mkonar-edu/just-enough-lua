Just Enough Lua
===============

_Just Enough Lua_ is an essentially (but not rigorously) correct crash course in Lua. It is intended to teach you just enough programming in Lua so that you can start to write games using open source game frameworks like [LÖVE](https://love2d.org/). It assumes you don't know anything about programming.

![Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)  
This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](https://creativecommons.org/licenses/by-nc-nd/4.0/)

_Just Enough Lua_ is Copyright © 2014-2021 Mithat Konar. All rights reserved.
