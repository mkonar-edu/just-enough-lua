% Tables: Arrays and Objects
% Mithat Konar
% Sep 19, 2021

Tables
-------
Tables in Lua are a universal solution for grouping data and functions together. We will look at using tables to make two commonly used grouping types: arrays and objects. In what follows we talk about arrays and objects as though they were different things, but to Lua there is no difference at the core: they are both just tables. How we use them is up to us.

Arrays using tables
--------------------
An **array** is a structure that lets you store a list of values where you access each value using a number called an **index**.

### Basics
You can create an array in Lua using an _array table literal_: a pair of curly brackets with a comma-separated list of values in between. Array literals can be assigned to variables.

```lua
local a = {2, 'Omar', false, 42}  -- Creates an array with 4 values in it.
local b = {}                      -- Creates an empty array.
```

Each "thing" in an array is an **element**. Elements in an array can be of any type.

You can access a single element in the array using square brackets and an **index**: a number that indicates the element in the array. Some people refer the the index as the **subscript**. Index numbering in Lua starts with 1 (one).^[Most popular programming languages start index numbering with 0, not 1. Lua is odd this way. Pascal is also an oddball and starts index numbering with 1.]

```lua
local myArray = {2, 'yo', 'OK', 42}    -- An array with 2 numbers and 2 strings.

print(myArray[1])     -- Prints 2
print(myArray[2])     -- Prints 'yo'
print(myArray[3])     -- Prints 'OK'
print(myArray[4])     -- Prints 42

myArray[2] = 'fantabulous'  -- Changes 'OK' to 'fantabulous'
print(myArray[2])     -- Prints 'fantabulous'
```

As you can see, you can use the index access to both read from the array and to overwrite an existing value in the array.

If you try to print out an array or any table, instead of seeing all the elements in the table as you might hope, you instead get the memory location where the table is stored.

```lua
local myArray = {2, 'yo', 'OK', 42}    -- An array with 2 numbers and 2 strings.
print(myArray)     -- Prints memory location of myArray (e.g. 0x19e12b0)
```

#### Array traversal

You can use a `for` loop to visit all the elements of the array and print them out (or do other things). This is called **traversing** the array.

```lua
local myArray = {2, 'yo', 'OK', 42}    -- An array with 2 numbers and 2 strings.

-- traverse myArray to print out element values.
for i = 1, 4 do
    print(myArray[i])
end
```

#### Number of elements

The `#` operator in front of an table name will tell you how many elements are in the table. This lets you traverse arrays without knowing how many elements are in them when you write the code.

```lua
local myArray = {2, 'yo', 'OK', 42}    -- An array with 2 numbers and 2 strings.

print('The number of elements in my array is ' .. #myArray .. '.')    -- #myArray returns 4

-- traverse myArray to print out element values.
for i = 1, #myArray do
    print(myArray[i])
end
```

#### Adding elements to the end of an array

You can add elements to an array by assigning something to an element beyond the end of the array:

```lua
local notes = {'do', 're', 'mi'}

notes[4] = 'fa'   -- adds a new element to array
print('length: ' .. #notes)

for i = 1, #notes do
    print(notes[i])
end
```

If you leave any "holes" (positions that are not defined), the `#` operator will only give you a count up to the first hole it sees:

```lua
local notes = {'do', 're', 'mi'}

notes[5] = 'so'       -- leaves a hole at 4

for i = 1, #notes do  -- #notes only goes up to the hole
    print(notes[i])
end

-- notes[5] is still there though:
print('notes[5]: '.. notes[5])  

notes[4] = 'fa'       -- fill in the hole

for i = 1, #notes do  -- now goes all the way
    print(notes[i])
end
```

#### Traversal with `ipairs`
Another way to traverse an array is to use `ipairs`. Depending on what you want to do, this can be a better way to do it.

You can think of an array as a mapping between an integer index and a value. Together, these make up a pair, or an `ipair` in Lua parlance. The following uses the `ipairs` concept to traverse an array.

```lua
local myArray = {2, 'yo', 'OK', 42}    -- An array with 2 numbers and 2 strings.

-- traverse myArray to print out element values.
for index, val in ipairs(myArray) do
    print('The value at index' .. index .. ' is ' .. val .. '.')
end
```

One advantage of traversing with `ipairs` is that you gain access to both the index and the value at the index position. The identifiers `index` and `val` in the above example are just variables; you can change these to any valid variable name.

Before you celebrate though, you'll want to know that you can't change the value of an element using `ipairs` traversal.

```lua
local myArray = {2, 'yo', 'OK', 42}    -- An array with 2 numbers and 2 strings.

-- try to change all elements to zero.
for index, val in ipairs(myArray) do
    val = 0
end

-- but nothing has changed.
for index, val in ipairs(myArray) do
    print('The value at index' .. index .. ' is ' .. val .. '.')
end
```

To actually change the value of an element, you need to use indexing notation.

### Table functions for arrays

Lua provides a number of functions for manipulating tables. Three of the most useful ones for manipulating arrays are described below.

#### table.insert()
You can use the `table.insert()` function to insert to an array. If you don't provide a position where to insert the new element, it will insert it at the end, which makes it a convenient way to add elements to an array.

```lua
local notes = {'do', 're', 'mi'}

table.insert(notes, 'so')     -- insert 'so' at end of table
table.insert(notes, 4, 'fa')  -- insert 'fa' at position 4

for i = 1, #notes do
    print(notes[i])
end
```

#### table.remove()
You can remove elements from an array using the `table.remove()` function. If you don't provide a position to remove, it will remove the last element.

```lua
local notes = {'do', 're', 'mi', 'fa', 'so'}

table.remove(notes, 4)  -- remove 'fa' at position 4
table.remove(notes)     -- remove 'so' at end of table

for i = 1, #notes do
    print(notes[i])
end
```

The `table.remove()` function also returns the element that was removed.

```lua
local notes = {'do', 're', 'mi', 'fa', 'so'}
local removedNote

removedNote = table.remove(notes, 4)
print('the removed note: ' ..  removedNote)
```

#### table.sort()

Use the `table.sort()` function to sort a table.

```lua
local notes = {'do', 're', 'mi', 'fa', 'so'}

table.sort(notes)
for i = 1, #notes do
    print(notes[i])
end
```

Objects using tables
---------------------
You can think of **objects** as being similar to arrays except that instead of using a numeric index to associate with values in the structure, you use some other kind of value. Most often, these are strings.

### Basics
In the example below, we use a Lua table literal to create an object that contains the properties for a couple balls we might use in a game.

```lua
local myBall= {
    ['size'] = 4,
    ['material'] = 'rubber',
    ['color'] = 'black'
}

local yourBall = {
    ['size'] = 5.5,
    ['material'] = 'glass',
    ['color'] = 'green'
}
```

The balls have the properties of `size`, `material`, and `color`. To access any individual property, we can use indexing syntax, but instead of using a number, we use the name of the property.

```lua
print('My ball is made of ' .. myBall['material'] .. '.')
print('Your ball is made of ' .. yourBall['material'] .. '.')
```

#### Key-value pairs
In the scheme above, the name of a property is called a **key**, th value it's associated with is its **value**, and together they make up a **key-value pair**.

Keys can be any valid Lua value. In fact, if you make the keys all sequential numbers starting from one, you'll actually make an array! The array syntax we have seen so far is actually just a shortcut for creating arrays the long way.

```lua
-- both a and b are arrays with elements 'uno', 'dos', 'tres', and 'cuatro'.
local a = {'uno', 'dos', 'tres', 'cuatro'}
local b = {[2] = 'dos', [1] = 'uno', [3] = 'tres', [4] = 'cuatro'}
```

Turning our attention back to objects, keys are almost always strings. This is so common that Lua has special syntax for this. If the key doesn't have any whitespace in it, you can eliminate the opening and closing square brackets and quotation marks in the literals.

```lua
local myBall= {
    size = 4,
    material = 'rubber',
    color = 'black'
}

local yourBall = {
    size = 5.5,
    material = 'glass',
    color = 'green'
}
```

And when accessing values associated with these keys, you can use **dot notation** instead of bracket notation.

```lua
print('My ball is made of ' .. myBall.material .. '.')
print('Your ball is made of ' .. yourBall.material .. '.')
```

#### Traversal with `pairs`

Above you saw how to traverse arrays using `ipairs`. This works when the keys in the table are integers, that is, when the table is an array. When the keys are _not_ integers, use `pairs` instead.

```lua
for key, val in pairs(yourBall) do
    print('The value for the key ' .. key .. ' is ' .. val .. '.')
end
```

The same limitation applies: you cannot change the values associates with the keys using `val`. However, you can use bracket (but not dot) notation.

```lua
-- change all the values in yourBall to "I don't know."
for key, val in pairs(yourBall) do
    yourBall[key] = "I don't know."
end
```

#### Adding properties

You can add properties to an already existing object using dot or bracket notation.

```lua
yourBall.texture = 'smooth'  -- give yourBall a new key-value pair
yourBall['mass'] = 42        -- ditto
```

### Properties and methods

The values in a Lua table can be any valid Lua value --- including functions! (Yes, functions are a kind of value in Lua.) At first this might seen strange, but grouping something's properties together with the functions that act on those properties forms the basis of what's called **object oriented-programming**.

When functions are made part of an object, they are often called **methods**. When you read documentation, you're likely to come across this term. Now you know what it means. Also, the properties and methods in an object are often together called the object's **members**. When objects are used only to hold data, they are sometimes called **structures** or **data objects**.

We won't go any deeper into methods or object-oriented programming here. But it's worth mentioning the above vocabulary because you're likely to encounter it when you begin to work with most game engines or other scriptable frameworks that use Lua.

## A game using a data object

Below is a game built using a data object. The `requisiteWarrior` data object bundles all the essential properties relevant to the `requisiteWarrior` into one package. Gameplay is simple loop that is easy to read thanks to the `requisiteWarrior` object and functions defined to operate on it.

The game is again quite lame. No skill, no strategy. Just pure chance. But it shows how objects can simplify writing games. In particular, thing about what your code might look like with and without objects if you had multiple warriors doing battle.

```lua
--[[ A "game" demonstrating the use of a table as a data object. ]]--

----------------------------------------------------------------
-- Entity definition(s)
----------------------------------------------------------------
--[[ 
    requisiteWarrior starts out with a health of 20.
    When they do battle, there is a 50/50 chance they will
    emerge victorious. Victory ramps up their health by 8,
    defeat down by 10. The requisiteWarrior dies when their 
    health drop below zero.
--]]
local requisiteWarrior = {
    health = 20,
    numberWins = 0,     -- counter for number of wins
    numberLosses = 0    -- counter for number of losses
}

----------------------------------------------------------------
-- Function definition(s)
----------------------------------------------------------------
--[[ doBattle makes a warrior do battle. If they win, their
     health and their number of wins is increased. If they loose
     they are reduced. 
     Returns whether the battle resulted in victory or not.
--]]
function doBattle(warrior)
    local isVictorious = math.random() > 0.5
    if isVictorious then
        warrior.health = warrior.health + 8
        warrior.numberWins = warrior.numberWins + 1
    else
        warrior.health = warrior.health - 10
        warrior.numberWins = warrior.numberLosses + 1
    end
    return isVictorious
end

--[[ isAlive returns whether a warrior is alive or not. ]]--
function isAlive(warrior)
    if warrior.health >= 0 then
        return true
    else
        return false
    end
end

----------------------------------------------------------------
-- Game variable(s)
----------------------------------------------------------------

local isWon   -- stores result of last battle.

----------------------------------------------------------------
-- Begin game. (Game ends when requisiteWarrior is no longer alive.)
----------------------------------------------------------------
math.randomseed(os.time())

print('Do battle!')

while isAlive(requisiteWarrior) do
    isWon = doBattle(requisiteWarrior)   -- battle!

    if isWon then
        print('Victory! Health is up to ' .. requisiteWarrior.health ..
              '.\n>>>[Enter] to do battle again.<<<')
        io.read()   -- wait for user to hit Enter key
    elseif isAlive(requisiteWarrior) then
        print('Defeat! Health is down to ' .. requisiteWarrior.health ..
              '.\n>>>[Enter] to do battle again.<<<')
        io.read()   -- wait for user to hit Enter key
    else
        print('Defeat. Alas, you have fought your last battle. :-(')
    end
end
```

<script>
window.onload = function () {
    activeNavLink('nav05');
}
</script>
