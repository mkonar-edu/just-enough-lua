% Introduction
% Mithat Konar
% Aug 17, 2021

Introduction
------------

### Intention
This is an essentially (but not rigorously) correct crash course in Lua. It is intended to teach you just enough Lua so that you can start to program games using game frameworks such as [LÖVE](https://love2d.org/) or [Roblox Studio](https://www.roblox.com/create). It assumes you don't know anything about programming.

### What is a computer system?
![](images/computer-sys.png)

Computer systems are made up of:

* Input
* Output
* Processing
* Storage  

Some programming concepts built on top of computer systems include:

* Functions
* Data structures (including tables)
* Objects

### Approach

We will explore each of the above in the context of Lua programming in the following order:

* Output
* Input
* Storage
* Processing
* Functions
* Tables
* Objects

When discussing initial concepts, we may have to introduce tiny bits of other concepts, but we'll keep it to an absolute minimum.

### Expectations
After you've learned some things about _processing_, you should be able to start doing some decent programming. By the time you've finished learning about _processing_, you'll be able to write simple text-based games. Be patient!

After you've learned about _functions_ and _tables_, you'll be able to write more complex text-based games. And when you have learned about _objects_, you'll be able to structure your code so that you can write even more complex games.

<script>
window.onload = function () {
    activeNavLink('nav01');
}
</script>
