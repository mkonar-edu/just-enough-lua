% Processing: Operations and Control Flow
% Mithat Konar
% Sep 6, 2021

Operations
----------
**Operations** are things you do with or to values that produce a new value. (Functions can do this too, but we'll talk about functions later.) Operations involve **operators** (the thing that specifies what to do) and **operands** (one or more values that will be used in the operation). Operands can be literals or variables.

There are many kinds of operations in Lua. Many important ones are discussed in this section.

### Arithmetic operators
An example of an operation in math is _`a + 3`_. The operator is _`+`_ and the operands are _`a`_ and _`3`_. Lua's arithmetic operators are summarized below.

```lua
local a, b
a = 10

b = a + 3     -- addition: b gets 13
b = a - 3     -- subtraction: b gets 7
b = a * 3     -- multiplication: b gets 30
b = a / 3     -- division: b gets 3.3333333333333
b = a // 3    -- floor division (round down): b gets 3 (Lua 5.3 and higher)
b = a % 3     -- modulus (remainder after division): b gets 1
b = a^3       -- exponentiation: b gets 1000.0
b = 9^0.5     -- exponentiation: b gets 3.0 (square root)
b = -a        -- negation: b gets -10
```

Note: The `//` operator (floor division) is available only in Lua versions 5.3 and higher.

#### Arithmetic order of operations
You can combine several operations in one expression:
```lua
a = 2 + 5 * 6 / 3 - 1   -- a gets 11
```

In situations like this, the order of operations is the same as in math:

* Multiplication and division before addition and subtraction.
* Left before right.

Also:

* Modulus is at the same level as multiplication and division.
* Exponentiation has higher precedence than multiplication and division.

You can use parenthesis to change the order of operations:
```lua
a = (2 + 5) * 6 / (3 - 1)   -- a gets 21.0
```

### Assignment
The **assignment operator** takes the value of whatever is on the right side of it and writes it into to the variable on the left side. Lua's assignment operator looks like the equals sign from math.
```lua
local a, b
b = 6           -- writes the value 6 into the variable b.
a = 3 + b       -- writes the value 9 into the variable a.
```

While Lua's assignment operator looks like the equals sign from math, they are **not** the same thing. Equals in math is a statement of fact: "_this_ equals _that_".  Assignment in computing is an operation: "Put the value of whatever is on the right into the variable on the left."

```lua
a = b + c   -- OK in math and Lua
b + c = a   -- NOT OK in Lua
```

#### Same variable on both sides of the assignment operator
One of the more useful things you can do with assignment is to put the same variable on both the left and right side of the assignment operator.
```lua
local a = 0
a = a + 2   -- value on right is 0 + 2, a becomes 2.

local numApples = 5
local eatenApples = 2
numApples = numApples - eatenApples   -- numApples becomes 3
```

You may not see _why_ this is useful right now, but you soon will.

### Relational operators
**Relational operators** (also called comparison operators) are used to compare two values. Their result is a boolean value.
```lua
local a, b
a = 2
b = 1000

print(a < b)   -- less than: result is true
print(a <= b)  -- less than or equal to: result is true
print(a > b)   -- greater than: result is false
print(a >= b)  -- greater than or equal to: result is false
print(a == 2)  -- equal to: result is true
print(a ~= b)  -- not equal to: result is true
```

### Logical operators
For complex control of logic, we will need **logical operators** --- which take one or more boolean values as operands and produce a boolean result. The usefulness of logical operations will make much more sense once we start to learn **control flow**.

An **`and`** operation will produce a true result only if _both_ its operands are true:
```lua
local a = 1
local b = 100
local c = 1000

print( (a < b) and (a < c) )   -- true  and true  -> true
print( (a > b) and (a < c) )   -- false and true  -> false
print( (a < b) and (a > c) )   -- true  and false -> false
print( (a > b) and (a > c) )   -- false and false -> false
```

An **`or`** operation will produce a true result if _either_ or _both_ its operands are true:
```lua
local a = 1
local b = 100
local c = 1000

print( (a < b) or (a < c) )   -- true  or true  -> true
print( (a > b) or (a < c) )   -- false or true  -> true
print( (a < b) or (a > c) )   -- true  or false -> true
print( (a > b) or (a > c) )   -- false or false -> false
```

A **`not`** operation takes one operand and produces the operand's opposite value:
```lua
local a = 1

print( not (a > 0) )   -- not true  -> false
print( not (a < 0) )   -- not false -> true
```

### String operators
The `..` operator when applied to strings **concatenates** them (i.e., puts them together).
```lua
local name, theMsg
name = 'Makani'
theMsg = 'My name is ' .. name .. '.'
print(theMsg)   -- prints "My name is Makani."
```

Relational operators also work with strings.
```lua
local str1 = 'aaaaa'
local str2 = 'zzzzz'

print(str1 < str2)    -- true
print(str1 == str2)   -- false
```

#### Mixing strings with other types
When using the `..` operator, if one of the operands is a number, then it will be converted to a string and concatenated.
```lua
local msgFragment, numBerries, theMsg
msgFragment = 'Number of blueberries: '     -- a string
numBerries = 42                             -- a number 
theMsg = msgFragment .. numBerries          -- string .. number makes a string
print(theMsg)                               -- "Number of blueberries: 42"
```

### Operator precedence

The **operator precedence** (i.e., the default order of operations) for the operators we've covered so far is shown below from highest to lowest.

|Operator                                  |Category |
|:---------------------------------------- |:------- |
|`^`                                       |exponentiation |
|`not   -`                                 |negation |
|`*     /     //    %`                     |multiplicative |
|`+     -`                                 |additive |
|`..`                                      |concatenation |
|`<     >     <=    >=    ~=    ==` &nbsp; |relational |
|`and`                                     |logical AND|
|`or`                                      |logical  OR |
|`=`                                       |assignment |

There are more operators in the Lua language, some of which we will explore as needed.

Control flow
------------
**Control flow** means, "Things you can do to affect the order in which things happen." Lua (like a lot of languages) provides three mechanisms for control flow:

* Sequence
* Selection
* Repetition

The really important thing about this is that _everything that can be computed can be computed using a combination of the three things above_.^[This was proven in the 1960s by Böhm and Jacopini in their structured program theorem.]

### Sequence
**Sequence** means statements are executed one after another. Sequence happens by default in Lua (and a lot of other languages).
```lua
local a = 3         -- this happens first...
print('foo')        -- then this...
a = a / 2           -- then this...
print('bar')        -- then this...
print(a)            -- then this...
print('Squirrel!')  -- then this...
```

There's not much more to say about that.

### Selection
A lot of programming logic has to deal with making decisions. **Selection structures** let you implement decisions by selectively executing code based on some kind of condition(s). We will look at three of Lua's selections structures: _if_, _if/else_, and _nested if/else_.

#### if
One of the most fundamental kinds of decisions you can make is: _if some condition is true, then do some action_.

* _If_ it's raining, _then_ bring an umbrella.
* _If_ it is dinner time, _then_ make dinner, set the table, and eat.

In Lua, this kind of logic is handled with the **`if`** statement.
```lua
local numMonsters

io.write('How many monsters are there? ')
numMonsters = io.read("*n")   -- read a number

if numMonsters == 0 then
    print('You have destroyed all monsters!')
end

print('You have the power!!!')
```

##### Formatting your code
Indenting the code between `then` and `end` makes the action stand out better than if it were not indented. This is considered a best practice. Also, lots of people like putting parenthesis around the condition to make it stand out better. You'll see us do this a lot. Finally, lots of people like putting `then` on its own line. Do whichever you think makes for the clearest code.

##### Executing multiple statements
If you want to execute more than one statement when the condition is true, then just put them all between `then` and `end`:
```lua
local today

io.write('What day is it? ')
today = io.read()   --read a string

if (today == 'friday') then
    print('Yay!')
    print('I get to sleep in tomorrow!')
end

print("Tomorrow, tomorrow, there's always tomorrow.")
```

#### if/else
A lot of times, you will want to do one thing if something is true and something else if it is false. One way to do this is to use two `if` statements that have inverted conditions.
```lua
local numMonsters
io.write('How many monsters are there? ')
numMonsters = io.read("*n")   -- read a number

if (numMonsters == 0) then
    print('You have destroyed all monsters!')
end

if (numMonsters ~= 0) then
    print('You must eliminate more mosters!')
end

print('You have the power!!!')
```

However, a _much_ better way is to use Lua's **if/else** structure:
```lua
local numMonsters

io.write('How many monsters are there? ')
numMonsters = io.read("*n")   -- read a number

if (numMonsters == 0) then
    print('You have destroyed all monsters!')
else
    print('You must eliminate more mosters!')
end

print('You have the power!!!')
```

It's better because:

* You only have to write the test condition once (which saves typing and creates less likelihood of writing a mistake).
* The program only has to run one test, which makes things run faster.

#### nested if/else
You can test for multiple conditions by putting if/else statements inside each other. This is called a **nested if/else**. Lua uses the keyword `elseif` to do this.
```lua
local numMonsters, msg

io.write('How many monsters are there? ')
numMonsters = io.read("*n")   -- read a number

if (numMonsters == 0) then
    msg = 'You have destroyed all monsters!'
elseif (numMonsters == 1) then
    msg = 'One to go!'
elseif (numMonsters < 5) then
    msg = 'Only a few left!'
else
    msg = 'Work harder!!!'
end

print(msg)
```

### First "game": number guessing
And now we can _finally_ write a simple game. It's quite lame, but it's a game (arguably). This is a simple game where we ask the player to guess a "magic number". If the user guesses correctly, they are rewarded with a nice message. If not, they are shamed.
```lua
--[[ An incredibly lame guessing game. ]]

local magicNum,  -- the number to be guessed
      yourNum    -- the user's guess

magicNum = 42    -- set the number to be guessed

-- get a guess from the user
io.write("I'm thinking of a number from 1 to 100.\nCan you guess what it is? ")
yourNum = io.read("*n")   -- read a number

-- compare the user's number to the magic number and message the user
if (yourNum == magicNum) then
    print('Congratulations!')
else
    print('Fail. All your base are belong to us.')
end
```

### Random numbers
Random numbers aren't directly related to control structures, but given the lameness of our game, it's a good time to talk about them.

To generate a sequence of random numbers in Lua, you can use the built-in `math.random()` function.

#### Generating random integers from 1 to a limit
To generate a series of random integers from 1 up to some integer, call `math.random(<upper-limit>)` repeatedly

```lua
print( math.random(10) )
print( math.random(10) )
print( math.random(10) )
print( math.random(10) )
print( math.random(10) )
```

Each time you call `math.random(10)` it will return a random number between 1 and 10. But if you run the above code more than once, you'll notice that it produces the _same_ sequence of random numbers every time. To get a new sequence of random numbers each time you run the program, you need to **seed** the random number generator. You only need to seed the random number generator once in your program. The classic way to seed it is to use the exact time your computer thinks it is. This is a lot easier than it sounds:
```lua
-- seed the random number generator with your computer's time.
math.randomseed( os.time() )

-- get a new random sequence of integers every time!
print( math.random(10) )
print( math.random(10) )
print( math.random(10) )
print( math.random(10) )
print( math.random(10) )
```

The `os.time()` function returns a value in seconds. This means if you run the above program within a second of its previous run, you'll still get the same sequence. In spite of this, `os.time()` works well enough for writing games.

Why doesn't the random number generator seed itself automatically? Because being able to have the same random number sequence is often helpful, for example when you are developing a program. 

#### Generating random integers from a lower to an upper limit
You can specify a start and end value for the random number range using two arguments.
```lua
-- seed the random number generator with your computer's time.
math.randomseed( os.time() )

-- get a random sequence of integers between -5 and 5
print( math.random(-5, 5) )
print( math.random(-5, 5) )
print( math.random(-5, 5) )
print( math.random(-5, 5) )
print( math.random(-5, 5) )
```

#### Generating random floating point numbers
If you call `math.random()` without any arguments, then it will produce a random number over the range _0 <= num < 1.0_.

```lua
-- seed the random number generator with your computer's time.
math.randomseed( os.time() )

-- get a random sequence of numbers between 0 and 1.0 (not including 1.0)
print( math.random() )
print( math.random() )
print( math.random() )
print( math.random() )
print( math.random() )
```

### First "game": slightly less bad?
We can now add some randomness to our first game to make it slightly less lame. The added code seeds the random number generator and then gets a random number between 1 and 100 to use as the `magicNum`.

```lua
--[[ An incredibly lame guessing game. Now with randomness! ]]

local magicNum,  -- the number to be guessed
      yourNum    -- the user's guess

math.randomseed( os.time() )  -- seed the generator
magicNum = math.random(100)   -- set the number to be guessed

-- get a guess from the user
io.write("I'm thinking of a number from 1 to 100.\nCan you guess what it is? ")
yourNum = io.read("*n")   -- read a number

-- compare the user's number to the magic number and message the user
if (yourNum == magicNum) then
    print('Congratulations!')
else
    print('Fail. All your base are belong to us.')
end
```

The number the user needs to guess is now randomized, but they only get one chance to get it right. That's not too fun. But there's a fix for this.

### Repetition

Another pattern that comes up a lot is repeating something over and over again:

* _While there is coffee left in my cup, take a sip of coffee._
* _For each item on my shopping list, find it in the store and put it in my basket._

This is called **repetition** or **looping** in programming. Lua has several structures for doing this.

#### while

The `while` structure is a direct expression of the idea: _while something is true, do some stuff_.
```lua
--[[ Parrot back back whatever the user enters until the get tired of it.]]
local something = ''

while (something ~= 'q') do
    io.write('Tell me something (or enter \'q\' to end): ')
    something = io.read()
    print('You said: ' .. something)
end
```

You can do repetition using any kind of condition, including those with numbers. Here's how to count to ten.
```lua
--[[ Print integers from 1 to 10. ]]
local i

i = 1               -- start counting at 1

while (i <= 10) do  -- continue until i is 11
    print(i)        -- print out the value of i
    i = i + 1       -- make i bigger by one 
end
```

And here's how you can add up numbers.
```lua
--[[ Compute the sum of integers from 1 to 100. ]]
local i, sum

i = 1               -- start counting at 1
sum = 0             -- the sum so far is zero

-- count through all the integers and add them to the sum
while (i <= 100) do
    sum = sum + i   -- and i to the sum so far
    i = i + 1       -- make i bigger by one
end

print('The sum of integers 1 to 100 is: ' .. sum);
```

The preceding are examples of repetition using a **counter**, which is the variable `i` in the examples. A counter is used to count how many times something has happened in a loop. In this case, it counts the number of times the loop has repeated. The name `i` is often used for counters.

The biggest danger with `while` loops is that you can create a situation where the repetition never ends. This is called an **infinite loop**.
```lua
--[[ Example of an infinite loop. ]]
local i = 1

-- i stays at 1 forever, so the condition is never false.
while (i <= 100) do
    print(i)
end

print("We'll never get here.");
```

#### Improving the number guessing game
Here is an improved version of the guessing game from earlier that lets the user guess until they've got it right. It uses a while loop to continue asking for guesses until it's correct.

```lua
--[[ An incredibly lame guessing game. More improved. ]]

local magicNum,         -- the number to be guessed
      yourNum,          -- the user's guess
      isGuessCorrect    -- whether the user has guessed correctly

magicNum = 42    --[[ set the number to be guessed. feel free to replace this
                      with a random number!! ]]

while (not isGuessCorrect) do
    -- get a guess from the user
    io.write("I'm thinking of a number from 1 to 100.\nCan you guess what it is? ")
    yourNum = io.read("*n")   -- read a number

    -- compare the user's number to the magic number and message the user
    if (yourNum == magicNum)
    then
        print('Congratulations!')
        isGuessCorrect = true
    else
        print('Fail. Try again.')
        isGuessCorrect = false
    end
end
```

#### counter-controlled for
Repeating something based on a counter typically follows the same pattern:
```lua
--[[ Print out the integers from 1 to 10. ]]
local i
i = 1               -- 1. initialize a counter variable

while (i <= 10) do  -- 2. test the counting condition
    print(i)        -- 3. do some stuff
    i = i + 1       -- 4. increment the counter
end
```

Another way to do this is to use Lua's **counter-controlled for** structure. It makes counter-controlled repetition easier to write and less likely to have bugs. It looks like:
```
for counter-variable = start-value, stop-value do
    <the-stuff-you-want-to-do>
end
```

For example:
```lua
for i = 1, 10 do
    print(i)
end
```

Here is the sum of integers program written using counter-controlled for:
```lua
--[[ Compute the sum of integers from 1 to 100. ]]
local i, sum
sum = 0

for i = 1, 100 do
    sum = sum + i   -- and i to the sum so far
end

print('The sum of integers 1 to 100 is: ' .. sum)
```

You can add a third argument to the for that tells it how many integers to skip between each repetition. Here's a loop that counts down from 10 in twos.
```lua
for i = 10, 0, -2 do
    print(i)
end
```

#### break
The **`break`** statement lets yout exit a repetition structure early. Here is our lame game again but using `break` to end the loop instead of using a loop controlling variable.

```lua
--[[ An incredibly lame guessing game. More improved. ]]

local magicNum,  -- the number to be guessed
      yourNum   -- the user's guess

magicNum = 42    --[[ set the number to be guessed. feel free to replace this
                      with a random number. ]]

while true do    -- this is a deliberate infinite loop!!
    -- get a guess from the user
    io.write("I'm thinking of a number from 1 to 100.\nCan you guess what it is? ")
    yourNum = io.read("*n")   -- read a number

    -- compare the user's number to the magic number and message the user
    if (yourNum == magicNum) then
        print('Congratulations!')
        break   -- the only way out is to hit this break!!
    else
        print('Fail. Try again.')
    end
end
```

You don't want to use `break` a lot, but it can be very useful in some situations.

#### repeat...until
Alongside `while` and `for`, Lua also offers the `repeat...until` structure. It's not used a lot, but you might see it. It's basically a while loop where the test happens at the _end_ of the loop rather than the start. Here's an example:
```lua
local done = ''

repeat
    io.write('Are we done yet? ')
    done = io.read()
until done == 'yes'
```

One of the consequences of having the test at the end of the loop is that the body of the loop is guaranteed to run at least once.

<script>
window.onload = function () {
    activeNavLink('nav03');
}
</script>
