% Output, Input, Storage, Data types
% Mithat Konar
% Aug 5, 2021

Output
------
### Output to the console
The **console** is a text-only area that's used for outputting text to. Even when writing graphic games, writing output to the console comes in handy for _debugging_ (fixing problems in the program).

You can write text to the console with the ``print()`` function.
```lua
print("Hello")
```

Notice the quotation marks around "Hello". You need those so Lua knows you want to print the string of characters "Hello" rather than, say, a variable `Hello`. We'll cover variables soon. The double quotation marks tell Lua that what is between them is a string of characters.

You can `print` numbers as well:
```lua
print(3)
print(21 + 3)
```

In fact, you can use `print()` to output many kinds of values.

Notice that these two are not the same:
```lua
print(21 + 3)
print("21 + 3")
```

The first calculates the result of 21 plus 3; the second prints out the string "21 + 3".

You can also use `io.write()` to print text to the console.  
```lua
io.write("Hello")
```

It behaves a lot like `print()`, but it gives you more control over how things are output. One important difference is that `io.write()` does not create a new line after it prints things out. For example, compare the difference between:
```lua
print("Hello.")
print("Welcome to Lua.")
```
and
```lua
io.write("Hello.")
io.write("Welcome to Lua.")
```    

### Escape sequences
What if you wanted to output something with double quotation marks in it? This doesn't work:
```lua
print("They said, "Yes".")
```

One way to solve this issue is to use a special sequence of characters (an **escape sequence**) that tells Lua that you want to output a double quote rather than use a quote to start or end a character string. That sequence is `\"`.
```lua
print("They said, \"Yes\".")

```

Lua has many escape sequences including:

* `\"`  double quote
* `\'`  single quote
* `\n`  start a new line
* `\t`  horizontal tab
* `\\`  backwards slash

You will see that all these escape sequences are pretty useful when we talk more about character strings.

### Printing more than one thing
Often times, you will want to print more than one thing in a single statement. There are a couple ways you can do this this.

The first way is to use Lua's **concatenation** operator. To concatenate two things means to put them together. So, concatenating "blue" and "berry" produces "blueberry". Lua's concatenation operator is `..` (two periods).

```lua
print("blue" .. "berry")
print("Did you have" .. " a " .. "blue" .. "berry" .. " " .. "today?")
```

Note that `..` does _not_ put any spaces between the items.

The concatenation operator works for numbers too.
```lua
print("I had " .. 42 .. " blueberries today.")
print("I had " .. 42 + 7 .. " blueberries today.")
print(42 .. 7)
```

The second way is to separate values with a comma. This approach separates values with a horizontal tab.
```lua
print("I had ", 42, "blueberries today.")
print("I had ", 42 + 7, "blueberries today.")
print(42, 7)
```

### Comments
While comments are not really related to output, we think this is a good place to introduce them. A **comment** is text you write in your program that is visible to you or a reader but that you tell Lua to ignore.

There are two kinds of comments in Lua:

* **single-line comments** --- everything from `--` to the end of the line is a comment.
    
    ```lua
    -- This is a single-line comment. It won't have any affect on the program.
    print("This is not a comment.")  -- But this is.
    ```
* **multi-line comments** --- everything between `--[[` and `]]` is a comment. It can span multiple lines.
    
    ```lua
    --[[ This is a multi-line comment. The comment ends
         after the closing double
         square 
         brackets. ]]
    ```
    It's common to put a pair of dashes before the closing `]]` and put it on its own line to make things look nicer.
    
    ```lua
    --[[
        The extra space and dashes 
        before the closing brackets
        help to set off mutli-line comments.
    --]]
    ```    
    There are other ways to write multi-line comments, but these are the easiest.

Comments are often used to add information about what your program is, who wrote the code and when it was written, as well as to describe what pieces of code are doing if it's not obvious.
```lua
--[[
    This bit of awesomeness was written by Mithat Konar.
    It was composed on Aug 5, 2021, a warm and smoldery day.
--]]

print("Hello, and welcome to my world.")  -- Greet the user.
```

Input
-----
### Input from the keyboard
To capture text input by the user typing into the console, you need to store the result of what they enter into a **variable**. We will discuss variable more completely later.

In both of the following examples, we create a variable named `resp` where we will store the input provided by the user, we tell the user what they should do with a **user prompt**, we get what they entered, and we then print out what they entered.
```lua
local resp
print('Enter something: ')
resp = io.read()
print('You said:')
print(resp)
```

When you run this, you'll notice some odd behavior. First, the prompt "Enter something:" appears on its own line. You may prefer to have the prompt appear on the same line as the user input. Second, right after "Enter something:" you'll see what the user entered echoed to the console before `print(resp)` executes.

Both of these can be solved by using `io.write()` instead of `print()` to output the user prompt.
```lua
local resp
io.write('Enter something: ')
resp = io.read()
print('You said:')
print(resp)
```


Storage
-------
### Variables
A **variable** is a place with a name in computer memory where you can store values. In the example below, we create (or **declare**) a variable named `x`, then we store the character string `'OMG!'` in `x` and output the value stored in `x` to the user. The special term `local` tells Lua to make _local_ variables with the name(s) that follow. We'll talk more about _local_ versus _global_ variables later.
```lua
local x = "OMG!"
print(x)
```

Notice how the above prints the _value_ stored in the variable, not the name of the variable.

The value stored in a variable can change. That's why it's called a variable!
```lua
local x = "OMG!"
print(x)  -- prints "OMG!"
x = "LOLZ!"
print(x)  -- prints "LOLZ!"
x = "Eat more fruit."
print(x)  -- prints "Eat more fruit."
```

### Variable names
In the preceding example, we gave our variable a very simple name, `x`. Variable names in Lua are usually longer than a single character. You should try to use variable names that help describe what is stored in the variable. This makes reading and writing a program much easier.
```lua
local myExclamation = "OMG!!"
local yourExclamation = "Heavens to Murgatroyd!" 

print(myExclamation)
print(yourExclamation)
```

### Rules of the name
Lua lets you name your variables anything you want as long as you stick to these rules:

* A variable's name can't be one of the [keywords reserved by Lua](http://www.lua.org/manual/5.4/manual.html#3.1). ^[This links shows the list of keywords for Lua version 5.4. They might be a little different depending on the version of Lua you're using.]
* Spaces in names are not allowed.
* The first character must be a Latin letter (a-z, A-Z) or an underscore ( _ ) character.
* Subsequent characters can be Latin letters, underscores ( _ ), or numbers.
* Names are cAsE sEnSiTiVe (`score` and `Score` are different).
* Examples:
    * OK: `num_lives`, `numLives`, `_lives`, `player1`
    * Bad: `num-lives`, `num lives`, `50cent`, `you&me`

The more pedantic term for names in programming is **identifier**. We will use name and identifier interchangeably here on out.

### Using variables
You can create a variable and give it an initial value:
```lua
local myExclamation = 'OMG!'
```
or just create it and assign it a value later on:
```lua
local myExclamation
-- other code --
-- other code --
-- other code -- 
myExclamation = 'OMG!'
```

You can create as many variables as you need in one go. Separate the variable names with commas.
```lua
local myExclamation, yourExclamation
myExclamation = 'OMG!'
yourExclamation = 'LOLZ!'
```

### Whitespace
The topic of whitespace isn't directly related to variables, but since we are talking about the details of using names, it's a reasonable time to bring it up. Spaces, line breaks, and horizontal tabs are categorized as **whitespace**. Whitespace lets Lua separate one element from another.

The rules of whitespace in Lua say that:

* There are places where you _must_ use whitespace. The following won't work without whitespace between `local` and `myExclamation`:
    
    ```lua
    local myExclamation = "Unicorns!"
    print(myExclamation)
    ```
* There are places where you _may_ use whitespace. Whitespace is optional between identifiers, operators, parenthesis and similar:
    
    ```lua
    local myExclamation="Unicorns!" -- whitespace removed (makes it harder to read)
    print ( myExclamation )         -- whitespace added (sometimes looks better)
    ```
* And there are places where you _cannot_ use any whitespace (e.g., within an identifier):
    
    ```lua
    local myExclamation = 'Rainbows!'
    print(my Exclamation)           -- won't work because of space in 'myExlamation'
    ```

Where you _may_ or _must_ use whitespace, you can use as much and of any kind you want.
    
```lua
local
                                        myExclamation 
                                    =
        "Unicorns!"
                                               print
                                  (
        myExclamation             )
```
😧 Use this flexibility to make your code look nice and clear, not sloppy or cryptic!!

Data types
----------
In the preceding example, we stored a string of characters in a variable.
```lua
local myExclamation
myExclamation = "OMG!"
```

Character strings (a.k.a. strings) are but one type of data that can be stored in variables. You can store the following **data types** in variables:

* strings 
* numbers
* boolean values
* The special value `nil`
* References to _functions_ and _tables_, which we will talk about later.
* References to _userdata_ and _threads_, which we will not talk about in this tutorial.

### Basic data types
#### strings
A string is a series of characters taken as a unit. In the following example, the variable `myString` stores a string.
```lua
local myString
myString = "I am not a robot."
```

The stuff on the right hand side of the equals sign is a **string literal** --- a string that is _not_ a variable. String literals are indicated by _double quotes_, _single quotes_, or _pairs of square brackets_.
```lua
local theirMessage = 'What is that buzz?'
local herMessage = "I have enough rope."
local hisMessage = [[Please call an ambulance.]]
```

If you need to use a single quote in a string literal, you can wrap it in double quotes and vice-versa. Or you can wrap them in pairs of square brackets. 
```lua
local myMessage, yourMessage, anotherMessage
myMessage = 'I said, "Hello."'
yourMessage = "That's cool!"
anotherMessage = [[They said, "That's not an avocado."]]
```

You can use escape sequences instead if you prefer.
```lua
myMessage = "I said, \"Hello.\""
yourMessage = 'That\'s cool!'
```

Lua has [limited support for Unicode](http://lua-users.org/wiki/LuaUnicode), which means you can store non-Latin characters in strings.

#### numbers
You can store both integers and floating point numbers, positive and negative in a Lua variable.
```lua
local luckyNumber, acceleration
luckyNumber = 7          -- integer (positive)
luckyNumber = -22        -- integer (negative)
acceleration = 31.23     -- floating point (positive)
acceleration = -0.33333  -- floating point (negative)
```
Note that
```lua
luckyNumber = 7
```
and
```lua
luckyNumber = '7'
```
are not the same thing. The first is a number, the second is a string.

There are several ways to write **numeric literals** including the following:
```lua
num = 3        -- integer
num = -1.234   -- simple floating point
num = 3.01e3   -- scientific notation
```

#### booleans
A boolean type can have one of two values: `true` or `false`.
```lua
local luaRocks, loveSucks
luaRocks = true
loveSucks = false
```

Note that
```lua
luaRocks = true
```
and
```lua
luaRocks = "true"
```
are not the same thing. The first is a boolean, the second is a string.

#### nil
A variable that has no value at all is `nil`. This can happen when a variable is declared but not yet set to anything.
```lua
local myNum
print(myNum)     -- outputs nil
myNum = 42
print(myNum)     -- outputs 42
```

You can also manually set a variable to `nil` if you want it to have "no value".
```lua
local myVar = 6  -- has the value six
myVar = null     -- now has no value
```

### Dynamic typing
Lua variables are **dynamically typed**. This means that they will store whatever type of data you want to store in them and it can change on the fly:
```lua
local myVar = 7  -- now a number
myVar = 'Yo!'    -- now a string
myVar = true     -- now a boolean
```

### Testing types
It's sometimes helpful when debugging a program to see what type of value is stored in a variable. The `type()` function will do this.
```lua
local myVar = 7
print(type(myVar))  -- outputs "number"
myVar = 'Yo!'
print(type(myVar))  -- outputs "string"
myVar = true
print(type(myVar))  -- outputs "boolean"
```

### Reading specific types
By default, `io.read()` will read an entire line of input _as a string_. This can lead to weird results when you want to capture the input as a number. You can tell `io.read()` to treat the input as a number by passing it `"*n"`:
```lua
local a, b
io.write("Enter a number: ")
a = io.read()      -- a will be a string

io.write("Enter another number: ")
b = io.read("*n")  -- b will be a number or nil
```

When you use the `"*n"` argument, if what the user enters  isn't a number, then `nil` is returned.

<script>
window.onload = function () {
    activeNavLink('nav02');
}
</script>
