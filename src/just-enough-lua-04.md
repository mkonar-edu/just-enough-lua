% Functions
% Mithat Konar
% Sep 13, 2021

Functions
---------
### Functions in mathematics
You're probably familiar with the concept of functions from mathematics. Given:

_f(x) = 3x + 1_

* _f_ is the **name of the function**
* _x_ is a **parameter**
* and the **return value** of the function is whatever is calculated as _3x + 1_.

Once the function has been definied, it must be **called** or **invoked** to use it. So,

* _f(2)_ returns the value _7_
* _f(11)_ returns the value _34_

and so on.

### Functions in programming

Functions in most programming languages are based on these basic ideas of functions in mathematics. However, functions in programming typically go beyond working on just numbers. For example, you've seen the `print()` function take text and numeric arguments and, rather than return a value, _do something_, namely _print_ the arguments to the command line.

```lua
print('My lucky number:', luckyNum)
```

Lua provides a range of functions for your use, some of which we'll touch on later in this chapter. But in addition to these predefined functions, you can also write and use your own so-called **programmer-defined functions**. Using programmer defined functions is a critical part of organizing your code so you can effectively write, debug, and reuse the code you write.

### Programmer-defined functions

#### Defining the function

Lua lets you define your own functions. Let's write a Lua version of the function above:
```lua
function f(x)       -- define a function f that has one parameter x
    local val       -- create a local variable to do some computing
    val =  3*x + 1  -- compute a value using whatever is passed in as x
    return val      -- end the function call and return the value
end
```

The keyword `function` states that what follows is a function definition. The name we are giving to our function is `f`, and between the first line and `end`, we define what we want our function to do. First we create a local variable to store a result, then we compute that value, and then we return it. The keyword `return` indicates the function should return after executing the statement, and the value it returns is the value of the expression on the right of `return`.

#### Calling the function

The definition won't do anything unless you **call** (or **invoke**) it:
```lua
function f(x)       -- define a function f that has one parameter x
    local val       -- create a local variable to do some computing
    val =  3*x + 1  -- compute a value using whatever is passed in as x
    return val      -- end the function call and return the value
end

print('f(2) is ' .. f(2))
print('f(11) is ' .. f(11))
```

As was the case with variable names, you want to use descriptive names for functions as well. Name rules for functions are the same as for variables. You might recognize the function _f(x) = 3x + 1_ as representing a line. If that was our intent, a better name for the function might be `myLine`. Let's make that change, add a comment before defining the function that says what the function does, and make some other small changes.

```lua
--[[ Compute the y value for my line given an x value.]]
function myLine(x)
    local val       -- create a local variable to do some computing
    val =  3*x + 1  -- compute a value using whatever is passed in as x
    return val      -- end the function call and return the value
end

print('The y value for my line when x is 2 is ' .. myLine(2))
print('The y value for my line when x is 2 is ' .. myLine(11))
```

When you call (or invoke) a function, you can pass in literal values, variables, or any other expression that evaluates to the expected type of value.

```lua
--[[ Compute the y value for my line given an x value.]]
function myLine(x)
    local val       -- create a local variable to do some computing
    val =  3*x + 1  -- compute a value using whatever is passed in as x
    return val      -- end the function call and return the value
end

local x1, y1, x2, y2
x1 = 2
x2 = 42
y1 = myLine(x1)
y2 = myLine(x2)

print('The y value for my line when x is ' .. x1  ..' is ' .. y1)
print('The y value for my line when x is ' .. x2  ..' is ' .. y2)
```

#### Make it a party

You can define as many functions as you need and have functions call other functions.
```lua
--[[ Return the sum of three numbers. ]]
function addThreeNumbers(a, b, c)
    local total = a + b + c
    return total
end

--[[ Return the average of three numbers. ]]
function averageThreeNumbers(x, y, z)
    local average = addThreeNumbers(y, z, x) / 3
    return average
end

-- Test the functions
local msg, tot, avg

tot = addThreeNumbers(6, 7, 9)
avg = averageThreeNumbers(6, 7, 9)

msg = 'The total is ' .. tot .. '.\n' ..
      'The average is ' .. avg .. '.'
print(msg)
```

#### Some examples

Functions can be simple as above or more complicated:
```lua
--[[ weeklyPay computes the weekly pay for someone given their total hours worked. ]]
function weeklyPay(numHours)
    local HOURLY_WAGE = 10.00
    local OVERTIME_FACTOR = 1.5
    local basepay = 0
    local overtime = 0
    
    if (numHours <= 40) then
        -- regular hours
        basepay = HOURLY_WAGE * numHours
    else
        -- overtime!
        basepay = HOURLY_WAGE * 40
        overtime = HOURLY_WAGE * OVERTIME_FACTOR * (numHours - 40)
    end
    
    return basepay + overtime
end

--[[ Ask the user for the number of hours they have worked this week and 
     calculate their pay.
--]]
local hours, myPay

io.write('How many hours did you work this week? ' )
hours = io.read("*n")
myPay = weeklyPay(hours)

print('Week\'s pay: ' .. myPay)
```

Your functions can take  more than one parameter. Add additional parameters in a comma-separated list.
```lua
--[[ weeklyPay computes the weekly pay for someone given their total hours worked
     and hourly wage.
--]]
function weeklyPay(numHours, hourlyWage)
    
    local OVERTIME_FACTOR = 1.5
    local basepay = 0
    local overtime = 0
    
    if (numHours <= 40) then
        -- regular hours
        basepay = hourlyWage * numHours
    else
        -- overtime!
        basepay = hourlyWage * 40
        overtime = hourlyWage * OVERTIME_FACTOR * (numHours - 40)
    end
    
    return basepay + overtime
end

--[[ Ask the user for the number of hours they have worked this week and 
     calculate their pay.
--]]
local wage, hours, myPay

io.write('What is your hourly wage? ' )
wage = io.read("*n")
io.write('How many hours did you work this week? ' )
hours = io.read("*n")
myPay = weeklyPay(hours, wage)

print('Week\'s pay: ' .. myPay)
```

Your functions can take zero parameters.
```lua
--[[ Calculate a magic number. ]]
function calulateMagicNumber()
    local magicNum
    local TEN = 10
    local ELEVEN = 11
    
    magicNum = 422 // TEN + TEN - 65 % ELEVEN
    return magicNum
end

--[[ Print out a magic number. ]]
local num
num = calulateMagicNumber()
print(num)
```

You don't even need to return a value. Functions that do not return a value are sometimes called **void functions**.
```lua
--[[ Print each player's score and who won. ]]
function printScores(player1score, player2score)
    print("Player 1's score: " .. player1score)
    print("Player 2's score: " .. player2score)
    
    if player1score > player2score then
        print('Player 1 wins!')
    elseif player1score < player2score then
        print('Player 2 wins!')
    else
        print("It's a tie!")
    end
end

-- code to test the printScores() function
local p1 = 99
local p2 = 100
printScores(p1, p2)
```

### Beyond math
As mentioned above, functions in programming go beyond just working on numbers. Any data type (not just numbers!) can be used for parameters and the return value.
```lua
--[[ formatNameWesternFormal returns a string representing the full
     name of an individual in formal Western format.
--]]
function formatNameWesternFormal(personalName, familyName)
    local fullName
    
    fullName = familyName .. ' ' .. personalName
    return fullName
end

-- Test formatNameWesternFormal()
local name
name = formatNameWesternFormal('Kareena', 'Kapoor')
print(name)
```

A function can just return a non-numeric value without accepting any parameters.

```lua
--[[ Prompt user to enter thier favorite color and return it. ]]
function getFavoriteColor()
    local color
    
    io.write('What is your favorite color? ')
    color = io.read()
    return color
end

-- Test getFavoriteColor()
local yourColor
yourColor = getFavoriteColor()
print('I like ' .. yourColor .. ' too!')
```

And functions with non-numeric parameters don't need to return values.
```lua
--[[ Display whether a voter is old enough to vote. ]]
function showVoterMsg(name, age)
    local msg
    
    msg = 'Hello, ' .. name .. '.'
    
    if (age >= 18) then
        msg = msg .. ' The voting booth is to the right.'
    else
        msg = msg .. ' You can vote when you are older.'
    end
    
    print(msg)
end

-- test showVoterMsg()
showVoterMsg('John Plummer', 17)
showVoterMsg('Ofra Zurer', 18)
```

### More predefined functions

Earlier you were introduced to the `math.random()`{.lua} and `math.randomseed()`{.lua} functions. Both of these belong to Lua's [math library](https://www.lua.org/manual/5.4/manual.html#6.7), and if you thought there might be more there than just random number support, you'd be right. Some others functions you'll find there include:

* `math.floor(x)`{.lua} ---  Returns the largest integral value less than or equal to x. .
* `math.ceil(x)`{.lua} --- Returns the smallest integer greater than or equal to a number.
* `math.log (x [, base])`{.lua} --- Returns the logarithm of x in the given base. The default for base is e (so that the function returns the natural logarithm of x). 

The `math` library also has a variable that has the value of pi (3.14159...) and all the trigonometric functions you're likely to need:

* `math.pi`{.lua} --- The value of π.
* `math.cos (x)`{.lua} --- Returns the cosine of x (assumed to be in radians). 
* `math.sin (x)`{.lua} --- Returns the sine of x (assumed to be in radians). 
* etc.

Take some time to familiarize yourself with these and any others that seem interesting to you.

### Lame Game with functions
Knowing what we now know about functions, we can now modify our guessing game to put various elements of the game into different functions.

```lua
--[[ An incredibly lame guessing game. With functions. ]]

----------------------------------------------------------------
-- Function definitions
----------------------------------------------------------------
--[[ Return a random number used in this game (inclusive). ]]
function randomNum()
   local num = math.random(100)
   return num
end

--[[ Prompt user for a guess and return it as a number. ]]
function getUserGuess()
    local guess
    io.write("I'm thinking of a number from 1 to 100.\nCan you guess what it is? ")
    guess = io.read("*n")   -- read a number
    return guess
end

----------------------------------------------------------------
-- Start the game
----------------------------------------------------------------
local magicNum,  -- the number to be guessed
      yourNum    -- the user's guess

math.randomseed( os.time() )    -- seed the random num. generator
magicNum = randomNum()          -- set the magic number

while true do
    yourNum = getUserGuess()

    -- compare the user's number to the magic number and message the user
    if (yourNum == magicNum) then
        print('Congratulations!')
        break
    else
        print('Fail. Try again.')
    end
end
```

### Scope

The following examples help to illustrate how **scope** works in Lua. **Scope** essentially means "where a variable can be accessed."  Consider the following example where we define a variable inside a function with same name as one outside the function:
```lua
-- Scope example 1: using local qualifiers --
function myFunc()
    local x = 99
    print(x)
end

local x = 1
print(x)
myFunc()
print(x)    -- the value of this x doesn't change
```

We can see that the `x` inside the function and the `x` outside the function are two different variables. We know this because the value of `x` outside the function didn't change after the function call completed. We can say that each `x` is _local_ to the area where it is declared.

If we make one small change---eliminating the ``local`` qualifiers when creating the variables---we will see a different result.
```lua
-- Scope example 2: without local qualifiers --
function myFunc()
    x = 99
    print(x)
end

x = 1
print(x)
myFunc()
print(x)    -- the value of this x changes
```

Now, the value of `x` outside the function does change after the function call. That's because in this case, both `x`'s refer to the _same_ `x`---one that is defined _globally_.

Variables can have **global** or **local** scope. If you create a variable without the `local` qualifier, it will have global scope, and we would refer to it as a **global variable**. If you use the `local` qualifier, it will have local scope, and we would refer to it as a **local variable**. So why is the distinction important?

#### The importance of `local`
If you use a name on the left side of an assignment that is not already the name of a variable that can be accessed, Lua will automatically create a new variable for you with that name. Variables created in this way are automatically global variables.
    
```lua
foo = 'Some value';    --[[ Creates a new global variable foo if 
                            foo doesn't already exist.]]   
```

At first, it seems like using global variables may not be such a bad idea. It might seem like would be convenient to be able to access all the variables of your program in any function you write. However, experience has shown this very quickly can lead to hard to detect errors that are also hard to fix. For example, it's only a matter of time before you accidentally name two variables that you meant to be different with the same name. You won't get any errors or warnings if you do this, but your program won't work as expected. This becomes even harder to manage when more than one person is working on the same program.

If you're in the habit of making all variables local, then it doesn't matter as much if two or more variables have the same name. This is because they will be created as different variables, and each will be "seen" only within the scope they are local to.

Having all your variables be global is a _really_ bad idea.

**Always, always, always use `local` unless you have a good reason not to**!

<script>
window.onload = function () {
    activeNavLink('nav04');
}
</script>
