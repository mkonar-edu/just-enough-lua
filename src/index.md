% Just Enough Lua
% Mithat Konar
% Aug 17, 2021

Notes for a crash course in [Lua](https://www.lua.org/).

* [1 | Introduction](just-enough-lua-01.html)
* [2 | Output, Input, Storage, Data types](just-enough-lua-02.html)
* [3 | Processing: Operations and Control Flow](just-enough-lua-03.html)
* [4 | Functions](just-enough-lua-04.html)
* [5 | Tables: Arrays and Objects](just-enough-lua-05.html)

<script>
window.onload = function () {
    activeNavLink('navhome');
}
</script>
