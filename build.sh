#!/bin/bash

SRC_DIR=./src
OUTDIR=../public             # relative to SRC_DIR
INCLUDES_DIR=../includes     # relative to SRC_DIR

# ------------
# simple html5
# ------------

cd $SRC_DIR
mkdir -p $OUTDIR

for name in $(ls -1 *.md)
do
    outname=$(basename -s .md $name).html
    pandoc -s --self-contained \
    -f markdown+smart \
    --toc \
    --toc-depth=3 \
    --include-in-header=$INCLUDES_DIR/copyright-meta.html \
    --css=$INCLUDES_DIR/codemirror.css \
    --css=$INCLUDES_DIR/style.css \
    --include-in-header=$INCLUDES_DIR/codemirror-compressed-link.html \
    --include-in-header=$INCLUDES_DIR/mfk.js.html \
    --include-before-body=$INCLUDES_DIR/navigation.html \
    --include-after-body=$INCLUDES_DIR/footer.html \
    --highlight-style=kate \
    -t html5 ${name} > $OUTDIR/$outname
done

# ------------------
# slidy (for slides)
# ------------------
#~ mkdir -p $OUTDIR-slides
#~ for name in $(ls -1 *.md)
#~ do
    #~ outname=$(basename -s .md $name).html
    #~ pandoc -s --self-contained \
    #~ --slide-level=3 \
    #~ --include-in-header=$INCLUDES_DIR/copyright-meta.html \
    #~ --css=$INCLUDES_DIR/codemirror.css \
    #~ --css=$INCLUDES_DIR/slidy-override.css \
    #~ --include-in-header=$INCLUDES_DIR/codemirror-compressed-link.html \
    #~ --include-in-header=$INCLUDES_DIR/mfk.js.html \
    #~ -f markdown+smart \
    #~ -t slidy ${name} > $OUTDIR-slides/$outname
#~ done
