#! /bin/bash

# Start a development server and open the home page in a browser.

SERVE_CMD="php -S localhost:8080"

# start server
xterm -T "$SERVE_CMD" -e $SERVE_CMD -t public/ &

# start browser
sleep 2
x-www-browser --new-window localhost:8080 &
